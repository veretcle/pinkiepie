> Pinkie Pie, who banished fear by giggling in the face of danger, represents the spirit of... laughter!

This is an example/template projet that uses iron (with router and bodyparser) and lapin crates to build a micro REST application able to take some input (via `POST` HTTP verb), check the content, validate it (must be XML) and put the result in the corresponding AMQP/RabbitMQ Exchange.

The objective is to build an HTTP server that binds on a local port and accept POST on an URI. If the posted data is an XML UTF-8 string, it is sent via RabbitMQ to a particular exchange/routing key/vhost via user/pass.

Whatever happens behind the scene (connection lost to RabbitMQ, channel closed, etc…), the Web part will still be able to respond :
* `200 (OK)` is the expected code if everything has been posted properly
* `417 (Expectation Failed)` if the POST body is empty
* `415 (Unsupported Media Type)` if the POST body is not UTF-8 XML
* `503 (Service Unavailable)` if something goes wrong with RabbitMQ

It is, of course, possible to parse the body as JSON/Simple text if necessary.

# Usage

```
USAGE:
    pinkiepie [FLAGS] [OPTIONS] --rabbitmq.host <HOST> --rabbitmq.user <USER>

FLAGS:
    -T, --rabbitmq.tls    RabbitMQ TLS (default false)
    -a, --rabbitmq.ack    ACK published messages (default false)
    -h, --help            Prints help information
    -V, --version         Prints version information

OPTIONS:
    -H, --rabbitmq.host <HOST>                 RabbitMQ host (no default)
    -v, --rabbitmq.vhost <VHOST>               RabbitMQ vhost (default /)
    -t, --rabbitmq.port <PORT>                 RabbitMQ port (default 5672 for plain, 5671 for TLS)
    -u, --rabbitmq.user <USER>                 RabbitMQ user (no default)
    -p, --rabbitmq.pass <PASS_FILE>            path to a file containing RabbitMQ password (default to
                                               /usr/local/etc/pinkiepie)
    -e, --rabbitmq.exchange <EXCHANGE>         RabbitMQ exchange (default nothing)
    -r, --rabbitmq.routingkey <ROUTING_KEY>    RabbitMQ routing key (default nothing)
    -P, --port <PORT>                          port to listen to (default 7878)
```

# Quirks

Everything is sync (as opposed to async), meaning every incoming POST request has to be processed completely before the thread is given back to iron router.

This means that it probably will not handle heavy load very well.

ACK will turn it *even slower*: every message has to be sent to RabbitMQ and the transaction had to be commited.

