use pinkiepie::*;

fn main() {
    let config = Config::new();

    run(config);
}
