// std
use std::sync::{Arc, Mutex};
use std::error::Error;
use std::fmt;

// clap
use clap::{Arg, App};

// iron
use iron::status::Status;
use iron::prelude::*;
use iron::Handler;

// router
use router::Router;

// lapin
use lapin::{
  BasicProperties, Connection, Channel, ConnectionProperties,
  options::*,
};

// xml
use xml::reader::EventReader;

/**********
 * Option parser and related options
**********/

/**
 * Struct config contains the configuration of the program
 **/
#[derive(Debug)]
pub struct Config {
    pub rabbitmq_host: String,
    pub rabbitmq_vhost: String,
    pub rabbitmq_tls: bool,
    pub rabbitmq_user: String,
    pub rabbitmq_pass: String,
    pub rabbitmq_port: u16,
    pub rabbitmq_exchange: Option<String>,
    pub rabbitmq_routingkey: Option<String>,
    pub rabbitmq_ack: bool,
    pub port: u16,
}

impl Config {
    /**
     * Parses the options with clap
     **/
    pub fn new() -> Config {
        let matches = App::new(env!("CARGO_PKG_NAME"))
                            .version(env!("CARGO_PKG_VERSION"))
                            .about("A daemon that uses iron and lapin to process POST requests to RabbitMQ messages")
                            .arg(Arg::with_name("rabbitmq.host")
                                 .short("H")
                                 .long("rabbitmq.host")
                                 .value_name("HOST")
                                 .help("RabbitMQ host (no default)")
                                 .takes_value(true)
                                 .required(true)
                                 .display_order(1))
                            .arg(Arg::with_name("rabbitmq.vhost")
                                 .short("v")
                                 .long("rabbitmq.vhost")
                                 .value_name("VHOST")
                                 .help("RabbitMQ vhost (default /)")
                                 .takes_value(true)
                                 .display_order(2))
                            .arg(Arg::with_name("rabbitmq.tls")
                                 .short("T")
                                 .long("rabbitmq.tls")
                                 .help("RabbitMQ TLS (default false)")
                                 .display_order(3))
                            .arg(Arg::with_name("rabbitmq.port")
                                 .short("t")
                                 .long("rabbitmq.port")
                                 .value_name("PORT")
                                 .help("RabbitMQ port (default 5672 for plain, 5671 for TLS)")
                                 .takes_value(true)
                                 .display_order(4))
                            .arg(Arg::with_name("rabbitmq.user")
                                 .short("u")
                                 .long("rabbitmq.user")
                                 .value_name("USER")
                                 .help("RabbitMQ user (no default)")
                                 .takes_value(true)
                                 .required(true)
                                 .display_order(5))
                            .arg(Arg::with_name("rabbitmq.pass")
                                 .short("p")
                                 .long("rabbitmq.pass")
                                 .value_name("PASS_FILE")
                                 .help("path to a file containing RabbitMQ password (default to /usr/local/etc/pinkiepie)")
                                 .takes_value(true)
                                 .display_order(6))
                            .arg(Arg::with_name("rabbitmq.exchange")
                                 .short("e")
                                 .long("rabbitmq.exchange")
                                 .value_name("EXCHANGE")
                                 .help("RabbitMQ exchange (default nothing)")
                                 .takes_value(true)
                                 .display_order(7))
                            .arg(Arg::with_name("rabbitmq.routingkey")
                                 .short("r")
                                 .long("rabbitmq.routingkey")
                                 .value_name("ROUTING_KEY")
                                 .help("RabbitMQ routing key (default nothing)")
                                 .takes_value(true)
                                 .display_order(8))
                            .arg(Arg::with_name("rabbitmq.ack")
                                 .short("a")
                                 .long("rabbitmq.ack")
                                 .help("ACK published messages (default false)")
                                 .display_order(9))
                            .arg(Arg::with_name("port")
                                 .short("P")
                                 .long("port")
                                 .value_name("PORT")
                                 .help("port to listen to (default 7878)")
                                 .takes_value(true)
                                 .display_order(10))
                            .get_matches();

        let rmq_path_file = matches.value_of("rabbitmq.pass").unwrap_or("/usr/local/etc/pinkiepie");
        let mut rmq_pass = std::fs::read_to_string(rmq_path_file).expect("Cannot read password from file!");
        rmq_pass.pop().expect("Cannot remove trailing \\n from password!");

        let mut rmq_port: u16;
        let rmq_tls: bool;

        if matches.is_present("rabbitmq.tls") {
            rmq_tls = true;
            rmq_port = 5671;
        } else {
            rmq_tls = false;
            rmq_port = 5672;
        }

        if matches.is_present("rabbitmq.port") {
            // can be unwrapped, we just checked that is was present
            rmq_port = matches.value_of("rabbitmq.port").unwrap().parse().expect("Cannot convert port number to u16");
        }

        Config {
            rabbitmq_host: matches.value_of("rabbitmq.host").unwrap().to_string(),
            rabbitmq_vhost: matches.value_of("rabbitmq.vhost").unwrap_or("%2f").to_string(),
            rabbitmq_tls: rmq_tls,
            rabbitmq_port: rmq_port,
            rabbitmq_user: matches.value_of("rabbitmq.user").unwrap().to_string(),
            rabbitmq_pass: rmq_pass,
            rabbitmq_exchange: matches.value_of("rabbitmq.exchange").map(|x| x.to_string()),
            rabbitmq_routingkey: matches.value_of("rabbitmq.routingkey").map(|x| x.to_string()),
            rabbitmq_ack: matches.is_present("rabbitmq.ack"),
            port: matches.value_of("port").unwrap_or("7878").parse().expect("Cannot convert port number to u16"),
        }
    }
}

/**********
 * Error handling
**********/

/**
 * local Error handle
 **/
#[derive(Debug)]
struct PinkiepieError {
    details: String,
}

impl PinkiepieError {
    fn new(msg: &str) -> PinkiepieError {
        PinkiepieError {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for PinkiepieError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl std::error::Error for PinkiepieError {
    fn description(&self) -> &str {
        &self.details
    }
}

/**********
 * iron routing and handling
***********/
/// Prints the current status of the application
fn status(_req: &mut Request) -> IronResult<Response> {
    let response_text = format!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
    Ok(response_handling(Status::Ok, &response_text))
}

/// Prints the Status Code on standard output and sends back the Response
fn response_handling(s: Status, m: &str) -> Response {
    println!("{}: {}", s, m);
    Response::with((s, m))
}

/// Checks that the file sent is a valid XML
fn valid_xml (xml: &str) -> Result<(), Box<dyn Error>> {
    let p = EventReader::from_str(xml);

    for l in p {
        if let Err(e) = l {
            return Err(Box::new(e));
        }
    }

    Ok(())
}

/// Struct containing the configuration of lapin and the handler for iron
struct RabbitMQPostHandler {
    addr: String,
    conn: Arc<Mutex<Option<Connection>>>,
    chan: Arc<Mutex<Option<Channel>>>,
    exch: Option<String>,
    rkey: Option<String>,
    ack: bool,
}

impl RabbitMQPostHandler {
    /// Initializes the structure itself
    fn new(host: &str,
           vhost: &str,
           tls: bool,
           port: u16,
           user: &str,
           pass: &str,
           exch: Option<&str>,
           rkey: Option<&str>,
           ack: bool) -> RabbitMQPostHandler {
        let mut amqp_uri: String;

        if tls {
            amqp_uri = "amqps://".to_string();
        } else {
            amqp_uri = "amqp://".to_string();
        }

        amqp_uri.push_str(&user);
        amqp_uri.push_str(":");
        amqp_uri.push_str(&pass);
        amqp_uri.push_str("@");
        amqp_uri.push_str(&host);
        amqp_uri.push_str(":");
        amqp_uri.push_str(&port.to_string());
        amqp_uri.push_str("/");
        amqp_uri.push_str(&vhost);

        RabbitMQPostHandler {
            addr: amqp_uri,
            conn: Arc::new(Mutex::new(None)),
            chan: Arc::new(Mutex::new(None)),
            exch: exch.map(String::from),
            rkey: rkey.map(String::from),
            ack: ack,
        }
    }

    /// Initializes the Connection
    fn init_conn(&self) -> Result<Connection, Box<dyn Error>> {
        Ok(Connection::connect(self.addr.as_ref(), ConnectionProperties::default()).wait()?)
    }

    /// Checks that the Connection is OK, reinitializes it if it’s not
    fn check_conn(&self) -> Result<(), Box<dyn Error>> {
        if let Ok(mut conn) = self.conn.clone().lock() {
            match *conn {
                None => {
                    // connection need to be initialized
                    *conn = Some(self.init_conn()?);
                },
                Some(ref c) => {
                    // checks if connection is ok
                    if !c.status().connected() {
                        // connection had ended up in error state, we need to reinitialize it
                        *conn = Some(self.init_conn()?);
                    }
                },
            };
        } else {
            return Err(Box::new(PinkiepieError::new("Cannot lock mutex on Connection")));
        }

        Ok(())
    }

    /// Initializes the Channel associated with the Connection
    fn init_chan(&self, c: &Connection) -> Result<Channel, Box<dyn Error>> {
        let chan = c.create_channel().wait()?;
        if self.ack {
            chan.tx_select().wait()?;
        }
        Ok(chan)
    }

    /// Checks that the Channel is OK, reinitializes it if it’s not
    fn check_chan(&self) -> Result<(), Box<dyn Error>> {
        if let Ok(mut chan) = self.chan.clone().lock() {
            match *chan {
                None => {
                    // channel needs to be created
                    if let Ok(conn) = self.conn.clone().lock() {
                        match conn.as_ref() {
                            Some(c) => *chan = Some(self.init_chan(c)?),
                            None => return Err(Box::new(PinkiepieError::new("Cannot unwrap Connection"))),
                        }
                    } else {
                        return Err(Box::new(PinkiepieError::new("Cannot lock mutex on Connection")))
                    }
                },
                Some(ref c) => {
                    // checks if channel is ok
                    if !c.status().is_connected() {
                        // recreate channel if necessary
                        if let Ok(conn) = self.conn.clone().lock() {
                            match conn.as_ref() {
                                Some(c) => *chan = Some(self.init_chan(c)?),
                                None => return Err(Box::new(PinkiepieError::new("Cannot unwrap Connection"))),
                            };
                        } else {
                            return Err(Box::new(PinkiepieError::new("Cannot lock mutex on Connection")))
                        }
                    }
                },
            };
        } else {
            return Err(Box::new(PinkiepieError::new("Cannot lock mutex on Channel")));
        }

        Ok(())
    }

    /// Checks Connection/Channel then acquire Channel lock and publish the intended message
    fn publish(&self, m: &str) -> Result<(), Box<dyn Error>> {
        self.check_conn()?;

        self.check_chan()?;

        // at this point both self.conn and self.chan are in a reliable state, we’re ready to
        // publish
        if let Ok(chan) = self.chan.clone().lock() {
            let chan = match chan.as_ref() {
                Some(c) => c,
                None => return Err(Box::new(PinkiepieError::new("Cannot unwrap Channel"))),
            };

            // sends a persistent message (delivery_mode = 2)
            chan.basic_publish(self.exch.as_deref().unwrap_or(""),
                                self.rkey.as_deref().unwrap_or(""),
                                BasicPublishOptions::default(),
                                m.as_bytes().to_vec(),
                                BasicProperties::default().with_delivery_mode(2))
                                .wait()?;

            if self.ack {
                chan.tx_commit().wait()?;
            }

        } else {
            return Err(Box::new(PinkiepieError::new("Cannot lock mutex on Channel")))
        }

        // at this point we cannot fail…
        Ok(())
    }
}

impl Handler for RabbitMQPostHandler {
    /// Generic Iron handle:
    /// * does bodyparsing of the POST request
    /// * publishes the Body if everything checks out
    /// This function is not supposed to be able to fail, thus all the errors are catched and
    /// turned into HTTP Return Code
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        let body_raw = match req.get::<bodyparser::Raw>() {
            Ok(Some(body)) => body,
            Ok(None) => return Ok(response_handling(Status::ExpectationFailed, format!("Body appears to be empty").as_str())),
            Err(e) => return Ok(response_handling(Status::UnsupportedMediaType, format!("Body cannot be parsed: {}", e).as_str())),
        };

        match valid_xml(&body_raw) {
            Ok(_) => (),
            Err(e) => return Ok(response_handling(Status::UnsupportedMediaType, format!("Body is not a valid XML: {}", e).as_str())),
        }

        match self.publish(&body_raw) {
            Ok(_) => (),
            Err(e) => return Ok(response_handling(Status::ServiceUnavailable, format!("Something went horribly wrong with AMQP: {}", e).as_str()))
        }

        Ok(response_handling(Status::Ok, "OK"))
    }
}

/**********
 * Global run function
**********/
pub fn run(config: Config) {
    let handler = RabbitMQPostHandler::new(&config.rabbitmq_host,
                                           &config.rabbitmq_vhost,
                                           config.rabbitmq_tls,
                                           config.rabbitmq_port,
                                           &config.rabbitmq_user,
                                           &config.rabbitmq_pass,
                                           config.rabbitmq_exchange.as_deref(),
                                           config.rabbitmq_routingkey.as_deref(),
                                           config.rabbitmq_ack);
    let mut router = Router::new();

    router.get("/status", status, "status");

    router.post("/v1/billing", handler, "root");

    Iron::new(router).http(format!("localhost:{}", config.port)).unwrap();
}

